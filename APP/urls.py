from django.conf.urls import include, url
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from entity.views import CTDetails , DetailsExtraField ,ContainerInstancesListview ,DetailOrg,CreateRe,AttachRelation
#from entity.views import RelationAttach
#from entity.views import AttachRelation


urlpatterns = [
	# Examples:
	# url(r'^$', 'APP.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),


	url(r'^admin/', include(admin.site.urls)),
	url(r'^organisationinfo/$', csrf_exempt(DetailOrg.as_view()), name='organisationDetails'),
	#url(r'^organisationinfo/(?P<id>\d+)$', csrf_exempt(DetailOrg.as_view()), name='put'),
	url(r'^container-types/$', csrf_exempt(CTDetails.as_view()), name='container_details'),
	url(r'^container-types/(?P<ct_id>\d+ )$', csrf_exempt(CTDetails.as_view()), name='container_updations'),
	url(r'^extra-fields/container-types/', csrf_exempt(DetailsExtraField.as_view()), name='show_extrafields'),
	url(r'^container-instances/container-types/', csrf_exempt(ContainerInstancesListview.as_view()), name='container-instances'),
	url(r'^container-instances/container-types/(?P<ct_id>\d+)', csrf_exempt(ContainerInstancesListview.as_view()), name='put'),
	url(r'^container-types/relation/', csrf_exempt(CreateRe.as_view()), name='creating_container_realtion'),
	url(r'^container-types/instances/(?P<ci_id>\d+)', csrf_exempt(AttachRelation.as_view()), name='attaching-relation'),
	]




