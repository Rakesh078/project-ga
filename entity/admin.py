from django.contrib import admin

# Register your models here.
from .models import  Organisation, User, CT, ExtraField, DataType, ContainerInstances, Relationship,RelationshipInstances

admin.site.register(Organisation)
admin.site.register(User)
admin.site.register(CT)
admin.site.register(ExtraField)
admin.site.register(DataType)
admin.site.register(ContainerInstances)
admin.site.register(Relationship)
admin.site.register(RelationshipInstances)


