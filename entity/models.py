from django.db import models
from django.utils.timezone import datetime

# Create your models here.
class Organisation(models.Model):

	ORG_STATUS = (
					(0, 'Active'),
					(1,'Inactive'),
					(2,'Progress'),
					(3,'Closed')

	)

	Admin=0
	Manager=1
	Accountant=2
	Sales=3
	Tech=4
	Production=5
	Marketing=6

	DESINATION = (
					('Admin','Admin'),
					('Manager','Manager'),
					('Accountant','Accountant'),
					('Sales','Sales'),
					('Tech','Tech'),
					('Production','Production'),
					('Marketing','Marketing')
	)
	org_id =models.AutoField(primary_key=True)
	name=models.CharField(max_length=255,null=True,blank=True)
	mobile_no=models.CharField(max_length = 255 ,  null=True , blank=True)
	email_id=models.EmailField(max_length=255 , null=True , blank=True)
	date_of_registration= models.DateTimeField(auto_now_add = True)
	country= models.CharField(max_length=255 )
	degination=models.CharField(max_length=55 , choices = DESINATION)
	status= models.IntegerField(choices = ORG_STATUS , default = 1)
	is_deleted= models.BooleanField(default = False)


	def __unicode__(self):
		return str(self.org_id) + '-' + str(self.name) + '-'+ str(self.country)




class User(models.Model):

	TITLE = (
			  ('Mr.' , 'Mr'),
			  ('Mrs' , 'Mrs'),
			  ('Miss', 'Miss')
		)

	user_id = models.AutoField(primary_key = True)
	title   = models.CharField(max_length=5 , choices=TITLE)
	first_name= models.CharField(max_length = 50)
	middle_name= models.CharField(max_length=50, null=True, blank=True)
	last_name= models.CharField(max_length=50, null = True, blank=True)
	mobile_no  = models.CharField(max_length = 255 , null=True, blank = True)
	dob        = models.DateTimeField(default=datetime.now(), blank=True)
	org        = models.ForeignKey(Organisation, on_delete = models.CASCADE ,null=True , blank=True)
	is_deleted =models.BooleanField(default=False)

	def __unicode__(self):
		return str(self.user_id) + '_' + str(self.first_name)

	@property
	def get_user_name(self):
		name = str(self.first_name)
		if self.middle_name:
			name += ' '+str(self.middle_name)
		name += ' '+str(self.last_name)
		return name

class CT(models.Model):
	ct_id = models.AutoField(primary_key=True)
	name=models.CharField(max_length=255, unique=True)
	created_n=models.DateTimeField(auto_now_add=True)
	updated_n=models.DateTimeField(auto_now=True)
	updated_by=models.ForeignKey(User, on_delete=models.CASCADE , related_name = 'users',null=True, blank=True)
	created_by=models.ForeignKey(User , on_delete=models.CASCADE , null=True, blank=True)
	is_deleted=models.BooleanField(default=False)

	def __unicode__(self):
		return str(self.ct_id) + '_' + str(self.name)

class ExtraField(models.Model):
	DATA_TYPE = ( ('INTEGER','INT'),
				  ('CHARACTER','CHAR'),
				  ('DATE','DATE'),
				  ('DECIMAL' ,'DECIMAL')
		)
	ef_id =models.AutoField(primary_key=True)
	name  = models.CharField(max_length =255)
	discription = models.CharField(max_length=255)
	dtype = models.CharField(max_length = 255 , choices = DATA_TYPE)
	ct = models.ForeignKey(CT , on_delete = models.CASCADE , null=True , blank=True)
	is_deleted = models.BooleanField(default = False)

	def __unicode__(self):
		return str(self.name) +''+ str(self.dtype)

class ContainerInstances(models.Model):
	ct = models.ForeignKey(CT, on_delete=models.CASCADE , null=True , blank=True)
	is_deleted=models.BooleanField(default=False)
	
 

	def __unicode__(self):
		return str(self.ct.name)




class DataType(models.Model):
	intd=models.IntegerField(default=0)
	chard=models.CharField(blank=True, null=True , max_length=255)
	date = models.DateTimeField(default=datetime.now(), null=True)
	decimald = models.DecimalField(default=0.0 ,max_digits=20, decimal_places =2)
	ef=models.ForeignKey(ExtraField,on_delete=models.CASCADE,null=True,blank=True)
	ci=models.ForeignKey(ContainerInstances,on_delete=models.CASCADE,null=True,blank=True)

	def __unicode__(self):
		return str(self.pk) + ''+str(self.intd)+ ' '+str(self.chard)+ ''+ str(self.decimald)+' '+str(self.ef.ct.name)

class Relationship(models.Model):
	OTO=1
	OTM=2
	MTO=3
	MTM=4
	RELATION_TYPE_CHOICES=(
							 (OTO,'One-to-One'),
							 (OTM,'One-to-Many'),
							 (MTO,'Many-to-One'),
							 (MTM,'Many-to-Many'))
	parent=models.ForeignKey(CT,related_name='source_relationship')
	child= models.ForeignKey(CT,related_name='destination_relationship')
	relation_type=models.SmallIntegerField(choices=RELATION_TYPE_CHOICES,default=OTO)
	is_deleted=models.BooleanField(default=False)

	def __unicode__(self):
		return str(self.parent.name)+'  '+str(self.child.name)+ ' '+str(self.get_relation_type_display())

	@property
	def is_oto(self):
		return True if self.relation_type==self.OTO else False

	@property
	def is_otm(self):
		return True if self.relation_type==self.OTM else False

	@property
	def is_mto(self):
		return True if self.relation_type==self.MTO else False

	@property
	def is_mtm(self):
		return True if self.relation_type==self.MTM else False

class RelationshipInstances(models.Model):
	collection_mtm=models.ManyToManyField(ContainerInstances,related_name='rel_instances',null=True,blank=True)
	parent_rel=models.ForeignKey(ContainerInstances,related_name='relation_otm',null=True,blank=True)

	def __unicode__(self):
		return str(self.parent_rel.ct.name)





	
	
	






