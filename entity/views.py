from django.shortcuts import render
#from django.db.models import Count

# Create your views here.
from django.shortcuts import render
from django.views.generic import View
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, QueryDict
from django.db import DatabaseError
from django.core.exceptions import MultipleObjectsReturned,ObjectDoesNotExist

import json

from . models import (Organisation ,User ,CT ,ExtraField ,
							DataType ,ContainerInstances, Relationship, RelationshipInstances)



class DetailOrg(View):
	#for retriving data items of Organisation
	def get(self,request):
		#print request
		#print request.scheme
		#print request.body
		print request.META
		#print request.read()
		#print request
		#b=request.GET['a']
		#print request.method
		#print type(a)
		#print b
		#print int(a)+int(b)

		
		#print request.path_info 
		#print request.content_type
		#print request.GET
		result = []
		orginfo = Organisation.objects.all()
		for org in orginfo:
			dict_res={}
			if not org.is_deleted:
				dict_res['org_id']=org.org_id
				dict_res['name']=org.name
				dict_res['mobile_no']=org.mobile_no
				dict_res['email_id']=org.email_id
				dict_res['date_of_registration']=str(org.date_of_registration)
				dict_res['country']=org.country
				dict_res['degination']=org.degination
				dict_res['status']=org.status
				result.append(dict_res)
		return HttpResponse(json.dumps(result), content_type="application/json")


	#for inserting data items of organisations
	def post(self , request ):
		#print request.POST.get('a')
		#print request.GET.get('a')
		#print request.GET
		ins=request.POST
		print QueryDict(request.body)
		#print ins.values()
		print ins
		#return HttpResponse("Sucess",status=200)										
		nm=ins['name']
		mn=ins['mobile_no']
		eid=ins['email_id']
		c=ins['country']
		dn=ins['degination']
		if nm=='' or mn==''or eid=='' or c=='' or dn=='':
			return HttpResponse('name , mobile_no,email_id,mobile_no,country,degination, mandatory fields')
		entry=Organisation(name=nm,mobile_no=mn,email_id=eid,country=c,degination=dn)
		entry.save()
		return HttpResponse('Sucess',status=200)
		


   #for updation data items using org_id
	def put(self,request):
		try:
			print "hii"
			id = request.GET.get('id','None') #url parameter can be passed
			#id=int(id)
			print id 
			update_req=QueryDict(request.body) #only bodies data are passed not url parameter passed
			print update_req
			print update_req['mobile_no']

			data=Organisation.objects.get(org_id=id)
			if "name" in update_req.keys():
				data.first_name=update_req["name"]
			if "mobile_no" in update_req.keys():
				data.middle_name=update_req["mobile_no"]
			if "email_id" in update_req.keys():
				data.last_name=update_req["email_id"]
			if "date_of_registration" in update_req.keys():
				data.mobile_num=update_req["date_of_registration"]
			if "country" in update_req.keys():
				data.admission_date=update_req["country"]
			if "degination" in update_req.keys():
				data.college_id=update_req["degination"]
			if "status" in update_req.keys():
				data.college_id=update_req["status"]
			data.save()
			return HttpResponse("updated",status=200)
		except ObjectDoesNotExist:
			return HttpResponse("obj not exit",status=500)


	 #for deletion of data items using org_id
	def delete(self,request):
		#print request.body
		abc = QueryDict(request.body)
		print abc
		id  = abc['id']
		print id
		try:
			obj = Organisation.objects.get(org_id = id)
			#obj.delete()
			if not obj.is_deleted:
				obj.is_deleted=True
				obj.save()
			return HttpResponse("sucessfully deleted")
		except ObjectDoesNotExist:
			return HttpResponse("obj not exit",status=200)


class CTDetails(View):
	def get(self,request):
		res=[]
		ctinfo=CT.objects.all()
		for ct in ctinfo:
			if not ct.is_deleted:
				dic={
					  
					  'ct_id':ct.ct_id,
					  'ct_name':ct.name,
					  'ct_created_on':str(ct.created_n),
					  'ct_updated_on':str(ct.updated_n),
					  'ct_updated_by':ct.updated_by.get_user_name,
					  'ct_created_by':ct.created_by.get_user_name
				}

				res.append(dic)
		return HttpResponse(json.dumps(res),content_type="application/json")

	def post(self,request):
		insert=request.POST
		name=insert['nm']
		user_id=insert['u_id']   #inserting user_id from user models according to it foreign key are inserted
		try:
			user_obj=User.objects.get(user_id=user_id)
			try:
				ct_obj=CT.objects.get(name=name)
				return HttpResponse("Container Type is already created try with next",status=200)
			except ObjectDoesNotExist:
				entry=CT(name=name , updated_by=user_obj , created_by=user_obj)
				entry.save()
				return HttpResponse("Container Created Sucessfully ",status=200)
		except ObjectDoesNotExist: 
			return HttpResponse("User Object Not Exist",status=500)

	def put(self,request):
		try:
			ct_id=request.GET.get('ct_id','None')
			update_req=QueryDict(request.body)
			update_obj=CT.objects.get(ct_id=ct_id)
			if not update_obj.is_deleted:
				if "name" in update_req.keys():
					update_obj.ct_name=update_req["name"]
			update_obj.save()
			print update_obj.ct_name
			return HttpResponse("sucessfully updated",status=200)

		except ObjectDoesNotExist:
			return HttpResponse("object is not exit ",status=500)
	def delete(self,request):
		try:
			del_id=request.GET.get('ct_id','None')
			#print del_id
			del_obj=CT.objects.get(ct_id=del_id)
			if not del_obj.is_deleted:
				del_obj.is_deleted=True
				del_obj.save()
				return HttpResponse("deleted sucessfully",status=200)
		except ObjectDoesNotExist:
			return HttpResponse("Already deleted ",status=500)

class DetailsExtraField(View):
	def get(self,request):
		ct_id=request.GET.get('ct_id','None')
		res=[]
		final=[]
		#print ct_id
		created_extra_obj=ExtraField.objects.filter(ct=ct_id)
		#print len(created_extra_obj)
		if len(created_extra_obj)!=0:

			if created_extra_obj:
				final.append(
				{
				  'container_type':created_extra_obj[0].ct.name
				})
				for obj_itr in created_extra_obj:
					if not obj_itr.is_deleted:
						res.append({
						  'extra_field':obj_itr.dtype

					})
				final.append(res)
				return HttpResponse(json.dumps(final),content_type="application/json")
		else:

			return HttpResponse("NO_Container_Created",status=200)


	def post(self,request):
		'''
		THis function Created one by one extrafield of particular container Types

		'''
		insert=request.POST
		container_name=insert['name']
		try:
			container_type_obj=CT.objects.get(name=container_name)

			if insert['INT']=='INTEGER':
				entry=ExtraField(dtype=insert['INT'],ct=container_name)
				entry.save()
			elif insert['CHAR']=='CHARACTER':
				entry=ExtraField(dtype=insert['CHAR'],ct=container_name)
				entry.save()
			elif insert['DATE']=='DATE':
				entry=ExtraField(dtype=insert['DATE'],ct=container_name)
				entry.save()
			else:
				entry=ExtraField(dtype=insert['DECIMAL'],ct=container_name)
				entry.save()
			return HttpResponse("ExtraField is created sucessfully",status=200)

		except ObjectDoesNotExist:
			return HttpResponse("container specified is not created ist create it ",status=200)


class ContainerInstancesListview(View):


	def get(self,request):
		ct_id=request.GET.get('ct_id','None')
		container_instances_obj=ContainerInstances.objects.filter(ct=ct_id)
		if len(container_instances_obj)!=0: #exception if container_instances_obj retuen null
			try:

				container_type_object=CT.objects.get(ct_id=ct_id)
				extarnal_list=[]
				internal_list=[]
				for instantobj in container_instances_obj:
					#print instantobj.id
					if instantobj:
						extarnal_list.append({

							"container_type":container_type_object.name,
							"data":""

							})
						data_type_object=DataType.objects.filter(ci=instantobj.id)
						if len(data_type_object)!=0:

							for data_type in data_type_object:
								if data_type.intd:
									value=data_type.intd
								elif data_type.chard:
									value=data_type.chard
								else:
									value=str(data_type.decimald)
								internal_list.append({
								"ef_id":data_type.ef.ef_id,
								"ef_name":data_type.ef.name,
								"ef_value":value,
								"id":data_type.id
								})
							extarnal_list.append(internal_list)
						else:
							return HttpResponse("DataTypeDoesNotExit",status=200)

				return HttpResponse(json.dumps(extarnal_list),content_type="application/json")
			except ObjectDoesNotExist:
				return HttpResponse("ContainerDoesNotExist",status=200)
		else:

			return HttpResponse("ContainerDoesNotExist",status=200)

	def post(self,request):
		'''
		CReating new Container Instances using ct_id
		This Api create container instances for every instances of container data

		'''
		insert=request.POST
		ct_id=insert['ct_id']
		print ct_id
		try:
			container_type_object=CT.objects.get(ct_id=ct_id)
			extra_field_obj=ExtraField.objects.filter(ct=ct_id)
			if len(extra_field_obj)!=0:
				print extra_field_obj[0].dtype
			
				entry=ContainerInstances(ct=container_type_object)
				entry.save()
				c_ins_id=entry.ct
				for extra_iterator in extra_field_obj:
					if extra_iterator.dtype=='INTEGER':
						d_obj=DataType(intd=request.POST['intd'], ef=extra_iterator , ci=entry)
					elif extra_iterator.dtype=='CHARACTER':
						d_obj=DataType(chard=request.POST['chard'], ef=extra_iterator , ci=entry)
					elif extra_iterator.dtype=='DATE':
						d_obj=DataType(date=request.POST['date'], ef=extra_iterator, ci=entry)
					else:
						d_obj=DataType(decimald=request.POST['decimald'], ef=extra_iterator, ci=entry)
					d_obj.save()

				return HttpResponse("ContainerInstances Created Sucessfully",status=200)
			else:
				return HttpResponse("ExtraFieldNotExist",status=200)
		except ObjectDoesNotExist:
			return HttpResponse("ContainerDoesNotExist",status=200)
	
	def put(self, request):
		ct_id=request.GET.get('ct_id','None')
		#print ct_id
		update_req=QueryDict(request.body)
		#print update_req.keys()
		container_instances_obj=ContainerInstances.objects.filter(ct=ct_id)
		#print container_instances_obj
		for instances_iterator in container_instances_obj:
			if not instances_iterator.is_deleted:
				#print instances_iterator.pk
				data_type_update=DataType.objects.filter(ci_id=instances_iterator.pk)
				print data_type_update[2].ef.dtype
				for data_type_iterator in data_type_update:
					#print data_type_iterator.ef.dtype
					if data_type_iterator.ef.dtype=='INTEGER' and update_req.keys()=='dtype':
						data_type_iterator.dtype=update_req['dtype']
						#print update_req['dtype']
					if data_type_iterator.ef.dtype=='CHARACTER' and update_req.keys()=='chard':
						data_type_iterator.chard=update_req['chard']
					if data_type_iterator.ef.dtype=='DATE' and update_req.keys()=='date':
						data_type_iterator.date=update_req['date']
					if data_type_iterator.ef.dtype=='DECIMAL' and update_req.keys()=='decimald':
						data_type_iterator.decimald=update_req['decimald']
						#print "hiii"

				data_type_iterator.save()
		return HttpResponse("updation done sucessfully",status=200)


class CreateRe(View):

	def post(self,request):
		parent_ct=request.GET.get('ct_idp','None')
		#print parent_ct
		#print "how are u"
		child_ct=request.GET.get('ct_idc','None')
		choice=request.POST
		relation_info={}
		try:
			parent_ct_obj=CT.objects.get(ct_id=parent_ct)
			child_ct_obj=CT.objects.get(ct_id=child_ct)
			relation_info['parent']=parent_ct_obj
			relation_info['child']=child_ct_obj
			if choice['exclusive'] and choice['multiple_allowed']:
				relation_info['relation_type']=Relationship.MTM
			elif choice['exclusive'] and (not choice['multiple_allowed']):
				relation_info['relation_type']=Relationship.OTM
			elif (not choice['exclusive']) and choice['multiple_allowed']:
				relation_info['relation_type']=Relationship.MTO
			else:
				relation_info['relation_type']=Relationship.OTO
			relation=Relationship(**relation_info)
			relation.save()
			return HttpResponse("RelationshipCreated",status=200)
		except ObjectDoesNotExist:
				return HttpResponse("ContainerDoesNotExist",status=200)

	def put(self,request):
		parent_ct=request.GET.get('ct_idp','None')
		print parent_ct
		child_ct=request.GET.get('ct_idc','None')
		update_choice=request.POST
		try:
			relation_obj=Relationship.objects.get(parent__ct_id=ct_idp, child__ct_id=ct_idc)
			relation_type=relation_obj.relation_type
			if (update_choice['exclusive'] and update_choice['multiple_allowed']) and (relation_type==Relationship.OTO or relation_type==Relationship.OTM or relation_type==Relationship.MTO):
				relation_obj.relation_type=Relationship.MTM
			elif (update_choice['exclusive'] and (not update_choice['multiple_allowed'])) and (relation_type==Relationship.OTO):
				relation_obj.relation_type=Relationship.OTM
			elif ((not update_choice['exclusive']) and update_choice['multiple_allowed']) and (relation_type==Relationship.OTO):
				relation_obj.relation_type=Relationship.MTO
			else:
				return HttpResponse("NOupdationAllowed",status=300)
			relation_obj.save()
			return HttpResponse("updation Sucessfully",status=200)

		except ObjectDoesNotExist:
			return HttpResponse("NO Relationship Exist")


class AttachRelation(View):
	'''
	This Api maintains the Attachemnt of child according to their RelationShip Type
	We are considering parent as unique and multiple childern can attach with parents 
	for m2m childern can be stored in any parent with out duplicate storage in particular parent i.e two c2's
	cannot be stored in same parents ,but can be stored in multiple parents so its m2m

	similarly for one to many we are saying if a child is attach with a particular parent then that child cannot
	be attach with other parents 

	for one to one one children have attach with only one parent if relation exist otherwise return 

	'''
	def post(self,request,ci_id):
		print ci_id
		parent_ci_id=ci_id
		parms=request.POST
		id_child=parms['child_ci_id'].split()
		for child in id_child:
			child_ci_id=child
			#child_ci_id=request.POST['child_ci_id']
			#print child_ci_id
			try:
				ct_obj_par=ContainerInstances.objects.get(pk=parent_ci_id)
				print ct_obj_par.ct.name
				ct_obj_child=ContainerInstances.objects.get(pk=child_ci_id)
				print ct_obj_child.ct.name
				relation_obj_par=Relationship.objects.get(parent__ct_id=ct_obj_par.ct.ct_id,child__ct_id=ct_obj_child.ct.ct_id)
				print relation_obj_par.is_otm
				print relation_obj_par.is_mto

				if relation_obj_par.is_mtm:
					try:
						relation_instance_obj=RelationshipInstances.objects.get(parent_rel__id=parent_ci_id)
						print relation_instance_obj
						for i in relation_instance_obj.collection_mtm.all():
							print i.id , type(i.id) , type(child_ci_id)
							if i.id==int(child_ci_id):
								return HttpResponse("RelationshipExist",status=200)

							else:
								flag=True
						if flag:
							relation_instance_obj.collection_mtm.add(ct_obj_child)
							return HttpResponse("AttachingRelationship",status=200)
					except ObjectDoesNotExist:
						obj=RelationshipInstances(parent_rel=ct_obj_par)
						obj.save()
						obj.collection_mtm.add(ct_obj_child)
						return HttpResponse("RelationshipInitaillyCreated",status=200)
				elif relation_obj_par.is_otm or relation_obj_par.is_mto:
					try:

						relation_instance_obj=RelationshipInstances.objects.get(parent_rel__id=parent_ci_id)
						print relation_instance_obj
						relation_instance_all=RelationshipInstances.objects.all()
						print relation_instance_all
						for obj in relation_instance_all:
							for i in obj.collection_mtm.all():
								print i.id
								if i.id==int(child_ci_id):
									return HttpResponse("RelationshipAlradyExist",status=200)
								else:
									flag=True
						if flag:
							relation_instance_obj.collection_mtm.add(ct_obj_child)
							return HttpResponse("CreatedRealtion",status=200)

					except ObjectDoesNotExist:
						obj=RelationshipInstances(parent_rel=ct_obj_par)
						obj.save()
						obj.collection_mtm.add(ct_obj_child)
						return HttpResponse("RelationshipInitaillyCreated",status=200)

				elif relation_obj_par.is_oto:
					try:

						relation_instance_obj=RelationshipInstances.objects.get(parent_rel__id=parent_ci_id)
						return HttpResponse("relation cant be created its alredy exist ",status=200)
					except ObjectDoesNotExist:
						obj=RelationshipInstances(parent_rel=ct_obj_par)
						obj.save()
						obj.collection_mtm.add(ct_obj_child)
						return HttpResponse("RelationshipInitaillyCreated",status=200)
				else:
					return HttpResponse("not allowed",status=200)
			except ObjectDoesNotExist:
				return HttpResponse("No Relationship Exit out",status=200)


	def get(self,request,ci_id):
		'''
		  displaying the child attach with particular parents with their extrafield and values 
		'''
		try:
			relation_instance_obj=RelationshipInstances.objects.get(parent_rel__id=ci_id)
			ext_list=[]
			ent_list=[]
			x=0
			for i in relation_instance_obj.collection_mtm.all():
				x=x+1
			ext_list.append({

					"parent_id":relation_instance_obj.id,
					"parent_type":relation_instance_obj.parent_rel.ct.name,
					"container_instance_id":relation_instance_obj.parent_rel.id,
					"no_of_child_attach":x
					})
			
			for mult_obj in relation_instance_obj.collection_mtm.all():
				ent_list.append({
					  
					  "container_instance_id":mult_obj.id,
					  "child_type":mult_obj.ct.name
					  })
				data_type_obj=DataType.objects.filter(ci__id=mult_obj.id)
				print type(data_type_obj)
				middle_lis=[]
				for data in data_type_obj:

					if data.intd:
						value=data.intd
					elif data.chard:
						value=data.chard
					else:
						value=str(data.decimald)
					middle_lis.append({
						"ef_id":data.ef.ef_id,
						"ef_name":data.ef.name,
						"ef_value":value,
						"id":data.id
						})
				ent_list.append(middle_lis)
			ext_list.append(ent_list)
			return HttpResponse(json.dumps(ext_list),content_type="application/json")
		except ObjectDoesNotExist:
			return HttpResponse("object doesnot exist",status=200)

			








	



				
























		



	






