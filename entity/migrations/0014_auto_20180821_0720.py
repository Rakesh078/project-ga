# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entity', '0013_auto_20180821_0525'),
    ]

    operations = [
        migrations.RenameField(
            model_name='relationshipinstances',
            old_name='collection_otm',
            new_name='parent_rel',
        ),
        migrations.AlterField(
            model_name='datatype',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 21, 7, 20, 20, 752672), null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 21, 7, 20, 20, 749149), blank=True),
        ),
    ]
