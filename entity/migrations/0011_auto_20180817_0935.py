# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entity', '0010_auto_20180817_0905'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='relationship',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='relationship',
            name='updated_by',
        ),
        migrations.AlterField(
            model_name='datatype',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 17, 9, 35, 30, 72691), null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 17, 9, 35, 30, 69746), blank=True),
        ),
    ]
