# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entity', '0012_auto_20180820_1146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datatype',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 21, 5, 25, 49, 638544), null=True),
        ),
        migrations.AlterField(
            model_name='relationshipinstances',
            name='collection_mtm',
            field=models.ManyToManyField(related_name='rel_instances', null=True, to='entity.ContainerInstances', blank=True),
        ),
        migrations.AlterField(
            model_name='relationshipinstances',
            name='collection_otm',
            field=models.ForeignKey(related_name='relation_otm', blank=True, to='entity.ContainerInstances', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 21, 5, 25, 49, 635510), blank=True),
        ),
    ]
