# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entity', '0011_auto_20180817_0935'),
    ]

    operations = [
        migrations.CreateModel(
            name='RelationshipInstances',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('collection_mtm', models.ManyToManyField(related_name='rel_instances', to='entity.ContainerInstances')),
                ('collection_otm', models.ForeignKey(related_name='relation_otm', to='entity.ContainerInstances')),
            ],
        ),
        migrations.AlterField(
            model_name='datatype',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 20, 11, 46, 51, 41821), null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 20, 11, 46, 51, 38998), blank=True),
        ),
    ]
