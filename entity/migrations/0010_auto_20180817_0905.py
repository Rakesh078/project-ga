# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entity', '0009_auto_20180817_0840'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datatype',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 17, 9, 5, 10, 595975), null=True),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='child',
            field=models.ForeignKey(related_name='destination_relationship', to='entity.CT'),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='parent',
            field=models.ForeignKey(related_name='source_relationship', to='entity.CT'),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 17, 9, 5, 10, 593245), blank=True),
        ),
    ]
