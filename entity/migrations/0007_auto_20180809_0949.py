# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entity', '0006_auto_20180809_0938'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datatype',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 9, 9, 49, 10, 997989), null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 9, 9, 49, 10, 995124), blank=True),
        ),
    ]
