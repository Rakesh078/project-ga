# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entity', '0002_auto_20180807_0701'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organisation',
            fields=[
                ('org_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('mobile_no', models.CharField(max_length=255, null=True, blank=True)),
                ('email_id', models.EmailField(max_length=255, null=True, blank=True)),
                ('date_of_registration', models.DateTimeField(auto_now_add=True)),
                ('country', models.CharField(max_length=255)),
                ('degination', models.CharField(max_length=55, choices=[(b'Admin', b'Admin'), (b'Manager', b'Manager'), (b'Accountant', b'Accountant'), (b'Sales', b'Sales'), (b'Tech', b'Tech'), (b'Production', b'Production'), (b'Marketing', b'Marketing')])),
                ('status', models.IntegerField(default=1, choices=[(0, b'Active'), (1, b'Inactive'), (2, b'Progress'), (3, b'Closed')])),
                ('is_deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.RemoveField(
            model_name='containerinstances',
            name='dt',
        ),
        migrations.AddField(
            model_name='datatype',
            name='ci',
            field=models.ForeignKey(blank=True, to='entity.ContainerInstances', null=True),
        ),
        migrations.AlterField(
            model_name='ct',
            name='created_by',
            field=models.ForeignKey(blank=True, to='entity.User', null=True),
        ),
        migrations.AlterField(
            model_name='ct',
            name='updated_by',
            field=models.ForeignKey(related_name='users', blank=True, to='entity.User', null=True),
        ),
        migrations.AlterField(
            model_name='datatype',
            name='chard',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='datatype',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 9, 6, 21, 44, 360333), null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 9, 6, 21, 44, 357503), blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='org',
            field=models.ForeignKey(blank=True, to='entity.Organisation', null=True),
        ),
    ]
