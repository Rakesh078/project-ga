# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entity', '0008_auto_20180809_1030'),
    ]

    operations = [
        migrations.CreateModel(
            name='Relationship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relation_type', models.SmallIntegerField(default=1, choices=[(1, b'One-to-One'), (2, b'One-to-Many'), (3, b'Many-to-One'), (4, b'Many-to-Many')])),
                ('is_deleted', models.BooleanField(default=False)),
                ('child', models.ForeignKey(related_name='source_relationship', to='entity.CT')),
            ],
        ),
        migrations.AlterField(
            model_name='datatype',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 17, 8, 40, 10, 748784), null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 17, 8, 40, 10, 746069), blank=True),
        ),
        migrations.AddField(
            model_name='relationship',
            name='created_by',
            field=models.ForeignKey(related_name='user_new_creation', to='entity.User'),
        ),
        migrations.AddField(
            model_name='relationship',
            name='parent',
            field=models.ForeignKey(related_name='destination_relationship', to='entity.CT'),
        ),
        migrations.AddField(
            model_name='relationship',
            name='updated_by',
            field=models.ForeignKey(related_name='user_updations', to='entity.User'),
        ),
    ]
