# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContainerInstances',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='CT',
            fields=[
                ('ct_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('created_n', models.DateTimeField(auto_now_add=True)),
                ('updated_n', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='DataType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('intd', models.IntegerField(default=0)),
                ('chard', models.CharField(default=b'null', max_length=255)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('decimald', models.DecimalField(default=0.0, max_digits=20, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='ExtraField',
            fields=[
                ('ef_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('discription', models.CharField(max_length=255)),
                ('dtype', models.CharField(max_length=255, choices=[(b'INTEGER', b'INT'), (b'CHARACTER', b'CHAR'), (b'DATE', b'DATE'), (b'DECIMAL', b'DECIMAL')])),
                ('is_deleted', models.BooleanField(default=False)),
                ('ct', models.ForeignKey(blank=True, to='entity.CT', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=5, choices=[(b'Mr.', b'Mr'), (b'Mrs', b'Mrs'), (b'Miss', b'Miss')])),
                ('first_name', models.CharField(max_length=50)),
                ('middle_name', models.CharField(max_length=50, null=True, blank=True)),
                ('last_name', models.CharField(max_length=50, null=True, blank=True)),
                ('mobile_no', models.CharField(max_length=255, null=True, blank=True)),
                ('dob', models.DateTimeField()),
                ('is_deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.AddField(
            model_name='datatype',
            name='ef',
            field=models.ForeignKey(blank=True, to='entity.ExtraField', null=True),
        ),
        migrations.AddField(
            model_name='ct',
            name='created_by',
            field=models.ForeignKey(to='entity.User'),
        ),
        migrations.AddField(
            model_name='ct',
            name='updated_by',
            field=models.ForeignKey(related_name='users', to='entity.User'),
        ),
        migrations.AddField(
            model_name='containerinstances',
            name='ct',
            field=models.ForeignKey(blank=True, to='entity.CT', null=True),
        ),
        migrations.AddField(
            model_name='containerinstances',
            name='dt',
            field=models.ForeignKey(blank=True, to='entity.DataType', null=True),
        ),
    ]
